$(function() {
  
 

    /*config*/
    var _launguageDataTable = {
        "sProcessing": "Traitement ...",
        "sLengthMenu": "Afficher _MENU_ records",
        "sZeroRecords": "Aucun résultat trouvé",
                "sEmptyTable": "Aucune donnée disponible dans ce tableau",
                "sInfo": " _START_ à _END_ d'un total de _TOTAL_ enregistrements",
                "sInfoEmpty": "Affichage des enregistrements de 0 à 0 sur un total de 0 enregistrements",
                "sInfoFiltered": "(filtrant un total de _MAX_ enregistrements)",
                "sInfoPostFix": "",
                "sSearch": "Recherche:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Chargement ...",
                "oPaginate": {            
            "Premier": "Premier",
                        "sLast": "Dernier",
                        "sNext": "Suivant",
                        "sPrevious": "Précédent"        
        },
                "oAria": {            
            "sSortAscending": ": Activer pour trier la colonne ascendante",
                        "sSortDescending": ": Activer pour ordonner la colonne dans l'ordre décroissant"        
        }    
    }
    setTimeout(() =>{$('.loading').fadeOut();} ,1000);
    

    /*list user*/
    $('#table-users').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
        ],
        responsive: true,
        "language": _launguageDataTable
    });


    /* alert closed */
    $(".myadmin-alert .close").click(function(event) {
        $(this).parents(".myadmin-alert").fadeToggle(350);
        return false;
    });

    /*delete user*/

    var tableUsers = $('#table-users').DataTable();
    $('.btn-delete-user').on('click', function(e) {
        e.preventDefault();
        idUser = $(this).attr('data-id');
        $.ajax({
            url: Routing.generate('user_delete', {
                id: idUser
            }),
            type: 'post',
            success: function(res) {
                tableUsers.row($('#' + idUser)).remove().draw();
                $('#modal-' + idUser).modal('hide');
                swal({
                    title: "Bon travail!",
                    text: "Votre operation a été passé avec succés!",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-success",
                    type: "success"
                });
            }
        });

    });

  /*delete template */
       var tablePage = $('#table-message-templates').DataTable();
    $('.btn-delete-template').on('click',function(e){
        e.preventDefault();
        idTemplate = $(this).attr('data-id');
        $.ajax({
              url:Routing.generate('delete_message_template',{id : idTemplate}),
              type:'POST',
              success: function(res){
                 tablePage.row($('#'+idTemplate)).remove().draw();
                 $('#modal-'+idTemplate).modal('hide');
                  swal({
                    title: "Bon travail!",
                    text: "Votre operation a été passé avec succés!",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-success",
                    type: "success"
                });
              }
        });
    });
    



    /*Adaptation knp_menu with theme*/
    $('.sidebar').find("li.current").addClass('active');
    $('.sidebar ul li').each(function() {
        if ($(this).hasClass('dropdown')) {
            var id = $(this).attr('href').replace('#', '');
            $(this).find('ul').wrap('<div id="' + id + '" class="collapse"></div>');
        }
    });
    $('.sidebar ul li .nav li').each(function() {
        if ($(this).hasClass('current')) {
            $(this).parent('ul').parent("div").parent("li").addClass('active');
            $(this).parent('ul').parent("div").parent("li").find("a:eq(0)").trigger('click');
        }
    });

    /* affectation user to boite d'annonce */

    $(".affecter-admin").on('click', function() {
        var boite = $(this).attr('data-typeB');
        var idBoite = $(this).attr('data-boiteId');
        var UserId = $("select[name='" + boite + "']").val();
        if (UserId == 0) {
            swal({
                title: "Attention",
                text: "Il faut selectionner un admin",
                timer: 2000,
                showConfirmButton: false
            });
        } else {
            $.ajax({
                url: Routing.generate('boiteAnnonce_affecter_admin', {
                    admin_id: UserId,
                    boite_id: idBoite
                }),
                type: 'POST',
                success: function(res) {
                    swal({
                        title: "Bon travail!",
                        text: "Votre operation a été passé avec succés!",
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-success",
                        type: "success"
                    });
                }
            });


        }


    });

    /*enabled desabled boxads */
    $("a.activted-boite").on('click', function(e) {
        e.preventDefault();
        var BoiteId = $(this).attr('data-boiteId');
        var Activeted = $(this).attr('data-activted');

        var message = (Activeted == 1) ? "Activer" : "Désactiver";
        swal({
            title: 'Vous voulez vraiment ' + message + ' cette boite !',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Oui,' + message,
            buttonsStyling: false
        }).then(function() {
            $.ajax({
                url: Routing.generate('boiteAnnonce_activeted', {
                    id: BoiteId
                }),
                type: 'POST',
                data: {
                    'check': Activeted
                },
                success: function(res) {
                    if (message == "Activer") {
                        $('.btn-act-' + BoiteId).hide();
                        $('.btn-dact-' + BoiteId).show();

                    } else {
                        $('.btn-act-' + BoiteId).show();
                        $('.btn-dact-' + BoiteId).hide();

                    }

                    swal({
                        title: message,
                        text: "Votre operation a été passé avec succés!",
                        type: 'success',
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    });
                }
            });
            return false;

        });



    });
    /* edit solde user */
    $('.btn-edit-solde').on('click',function(){
        var idUser = $(this).attr('data-id');
        var solde  = $('#soldeCurrent-'+idUser).val();
        $.ajax({
            url:Routing.generate('users_edit_solde',{ id : idUser}),
            type: 'POST',
            data:{'solde':solde},
            success:function(){
                 $('#modal-solde-' + idUser).modal('hide');
                 $('table td span#row-solde-'+ idUser).html(solde+' DH');
                  swal({
                        title: 'Modifier Solde',
                        text: "Votre operation a été passé avec succés!",
                        type: 'success',
                        confirmButtonClass: "btn btn-success",
                        buttonsStyling: false
                    });
            },
        });


    });





    /******************* Other JS Script *******************/
      




     var tablePack = $('#table-Pack').DataTable();
    $('.btn-delete-pack').on('click', function(e) {
         
        e.preventDefault();
        idPack = $(this).attr('data-id');
        $.ajax({
            url: Routing.generate('pack_delete', { id: idPack }),
            type: 'post',
            success: function(res) {
                tablePack.row($('#' + idPack)).remove().draw();
                $('#modal-' + idPack).modal('hide');
                swal({
                    title: "Bon travail!",
                    text: "Votre operation a été passé avec succés!",
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-success",
                    type: "success"
                });
            }
        });

    });




});