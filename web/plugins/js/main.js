   $(function(){
      $(document).on('click', '[data-href]', function () {
        var url = $(this).data('href');
        if (url && url.length > 0) {
            document.location.href = url;
            return false;
        }
      });
    });