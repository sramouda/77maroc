<?php
namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Session\Session;
class RedirectionListener{
	
	private $container;
	private $router; 
	private $securityContext;
	private $session;


	public function __construct(ContainerInterface $container,Session $session){
		$this->container = $container;
		$this->router    = $container->get('router');
		$this->securityContext = $container->get('security.token_storage'); 
		$this->session = $session;
		
	}

	public function onkernelRequest(GetResponseEvent $event){
		$routeCurrent = $event->getRequest()->attributes->get('_route');
	 	$CheckRoutes = [
            'dipost_ad',
         ];
		if(in_array($routeCurrent,$CheckRoutes)){
				if(!is_object($this->securityContext->getToken()->getUser())) {
					$this->session->getFlashBag()->add('info',"il faut connecter pour pouvoir Déposer une annonce");
					$event->setResponse(new RedirectResponse($this->router->generate('account_login')));
				}
		        	
       		}
	}

}