<?php
namespace AppBundle\Services;

use AppBundle\Entity\Ad;
use AppBundle\Entity\BoxAds;

class AutoDetectedBoiteAnnonce
{

    private $em;

    public function __construct($EntityManager)
    {
        $this->em = $EntityManager;
    }

    public function detectedBoite($check = false, $idBoite = null)
    {
        $idsBoxAd = $this->getLastAdsIds(count($this->getActivatedBoites()));
        $idsBox   = $this->getActivatedBoites();
        if ($check == false) {
            $list = array_merge(array_diff($idsBoxAd, $idsBox), array_diff($idsBox, $idsBoxAd));

            if (empty($list)) {
                return end($idsBoxAd);

            } else {
                return $list[0];
            }
        } else {

            $idsBoxAd = array_diff($idsBoxAd, array($idBoite));
            $idsBox   = array_diff($idsBox, array($idBoite));
            if (empty($list)) {
                return end($idsBoxAd);

            } else {
                return $list[0];
            }

        }

    }

    public function getActivatedBoites()
    {
        $box = $this->em->createQueryBuilder('boxad')
            ->select('boxad.id')
            ->from(BoxAds::class, 'boxad')
            ->where('boxad.boitecheck = :btc')
            ->setParameter('btc', true)
            ->getQuery()->getArrayResult();
        return $this->getsingleArray($box);
    }
    public function getLastAdsIds($limit = 1)
    {
        $listIds = $this->em->createQueryBuilder('ad')
            ->select('ad.boxad')
            ->from(ad::class, 'ad')
            ->where('ad.status is NULL')
            ->orderBy('ad.id', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()->getArrayResult();
        return $this->getsingleArray($listIds);

    }
    public function getsingleArray($array)
    {
        $list = array();

        foreach ($array as $item) {
            foreach ($item as $key => $value) {
                array_push($list, $value);
            }
        }

        return array_unique($list);
    }

}
