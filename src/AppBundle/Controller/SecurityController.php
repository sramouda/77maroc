<?php

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

class SecurityController extends BaseController
{

    public function renderLogin(array $data)
    {

        $requestAttributes = $this->container->get('request_stack')->getCurrentRequest();

        if ($requestAttributes->get('_route') == 'dashboard_login') {
            $template = sprintf('Dashboard/Security/login.html.twig');

        } else {
            $template = sprintf('FOSUserBundle:Security:login.html.twig');
        }

        return $this->container->get('templating')->renderResponse($template, $data);

    }

}
