<?php

namespace AppBundle\Controller;


use AppBundle\Entity\PublicityPosition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();


        $lastAds      = $em->getRepository('AppBundle:Ad')->findLatestAds(20);
        $publicityTop = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_PRINCIPALE,
            'position' => PublicityPosition::PAGE_PRINCIPALE_UNDER_RECHERCHE,
        ]);
        $publicityBottom = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_PRINCIPALE,
            'position' => PublicityPosition::PAGE_PRINCIPALE_UNDER_ANNONCES,
        ]);

        return $this->render('Default/index.html.twig', [
          
            'lastAds'         => $lastAds,
            'publicityTop'    => $publicityTop,
            'publicityBottom' => $publicityBottom,

        ]);
    }


}
