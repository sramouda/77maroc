<?php
namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\MessageTemplate;
use AppBundle\Entity\MessageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/messages-template")
 */

class MessageTemplateController extends Controller
{
    /**
     * @Route("/", name="index_message_template")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_MESSAGE_TEMPLATE')")
     */
    public function indexAction(Request $request)
    {

        return $this->render('Dashboard/MessageTemplate/messageTemplate.html.twig');
    }

    /**
     * @Route("/validation",name="message_validation")
     * @Route("/paiment"   ,name="message_paiment")
     * @SEcurity("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_MESSAGE_TEMPLATE')")
     */
    public function messageTemplateAction(Request $request)
    {
        if ($request->get('_route') == 'message_validation') {
            $type       = MessageType::ANNONCE_VALIDER;
            $currentURL = 'message_validation';
        } else {
            $type       = MessageType::PAIMENT_OPTIONS;
            $currentURL = 'message_paiment';
        }
        $em              = $this->getDoctrine()->getManager();
        $messageTemplate = $em->getRepository('AppBundle:MessageTemplate')->messageType($type);

        $form = $this->createForm('AppBundle\Form\MessageTemplateType', $messageTemplate, ['currentURL' => $currentURL]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Votre operation a été passé avec succéss');
            return $this->redirectToRoute($form['currentURL']->getData());
        }
        return $this->render('Dashboard/MessageTemplate/messageTemplateValidationOrPaiment.html.twig', ['form' => $form->createView()]);
    }

    /**
     *@Route("/deletions",name="list_message_supprissions")
     *@Route("/reject",name="list_message_refuser")
     *@Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_MESSAGE_TEMPLATE')")
     */
    public function listMessageTemplateAction(Request $request)
    {
        if ($request->get('_route') == 'list_message_supprissions') {
            $type = MessageType::ANNONCE_SUPPRIMER;
        } else {
            $type = MessageType::ANNONCE_REFUSER;
        }

        $em               = $this->getDoctrine()->getManager();
        $messagestemplate = $em->getRepository('AppBundle:MessageTemplate')->messageType($type, true);
        return $this->render('Dashboard/MessageTemplate/SupprissionOrRefuser/index.html.twig', ['templates' => $messagestemplate]);

    }

    /**
     *@Route("/deletions/add",name="add_message_supprissions")
     *@Route("/reject/add",name="add_message_refuser")
     *@Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_MESSAGE_TEMPLATE')")
     */
    public function addMessageTemplateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->get('_route') == 'add_message_supprissions') {
            $type        = $em->getRepository('AppBundle:MessageType')->findOneBy(['type' => MessageType::ANNONCE_SUPPRIMER]);
            $redirection = 'list_message_supprissions';
        } else {
            $type        = $em->getRepository('AppBundle:MessageType')->findOneBy(['type' => MessageType::ANNONCE_REFUSER]);
            $redirection = 'list_message_refuser';
        }
        $template = new MessageTemplate();
        $form     = $this->createForm('AppBundle\Form\MessageTemplateType', $template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $template->setMessagetype($type);
            $em->persist($template);
            $em->flush();
            $this->addFlash('success', 'Votre a été passé avec succés');
            return $this->redirectToRoute($redirection);
        }

        return $this->render('Dashboard/MessageTemplate/SupprissionOrRefuser/addTemplate.html.twig', ['form' => $form->createView()]);

    }
    /**
     *@Route("/deletions/{id}/edit",name="edit_message_supprissions")
     *@Route("/reject/{id}/edit",name="edit_message_refuser")
     *@Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_MESSAGE_TEMPLATE')")
     */
    public function editMessageTemplateSupprimerAction(Request $request, MessageTemplate $template)
    {
        if ($request->get('_route') == 'edit_message_supprissions') {
            $redirection = 'list_message_supprissions';

        } else {
            $redirection = 'list_message_refuser';
        }
        $form = $this->createForm('AppBundle\Form\MessageTemplateType', $template);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre a été passé avec succés');
            return $this->redirect($this->generateUrl($redirection));
        }

        return $this->render('Dashboard/MessageTemplate/SupprissionOrRefuser/editTemplate.html.twig', ['form' => $form->createView()]);

    }
    /**
     * @Route("/{id}/delete",options= {"expose" = true},
     * condition="request.isXmlHttpRequest()",name="delete_message_template")
     */
    public function deleteMessagetemplateAction(Request $request, MessageTemplate $template)
    {
        if ($template) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($template);
            $em->flush();
            return new JsonResponse(['success' => true]);
        } else {
            return new JsonResponse(['success' => false]);
        }

    }

}
