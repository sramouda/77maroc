<?php
namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\PaidOptions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/paidoptions")
 */
class PaidOptionsController extends Controller
{
    /**
     * @Route("/",name="paidoptions_list_index")
     *
     */
    public function indexAction(Request $request)
    {
        //     $em = $this->getDoctrine()->getManager();
        //     $always_in_lead = $em->getRepository('AppBundle:PaidOptions')->findOneBy(['name' => 'always_in_lead']);
        //     $renewal = $em->getRepository('AppBundle:PaidOptions')->findOneBy(['name' => 'renewal']);
        //     $attractive_ad = $em->getRepository('AppBundle:PaidOptions')->findOneBy(['name' => 'attractive_ad']);

        //     $always_in_lead_form = $this->createCreateForm($always_in_lead);
        //     $renewal_form = $this->createCreateForm($renewal);
        //     $attractive_ad_form = $this->createCreateForm($attractive_ad);

        //     return $this->render("Dashboard/PaidOptions/PaidOptions.html.twig",[
        //                         'always_in_lead_form' =>
        //                          $always_in_lead_form->createView() ,
        //                         'always_in_lead' => $always_in_lead,

        //                         'renewal_form' =>
        //                         $renewal_form->createView(),
        //                         'renewal' => $renewal,

        //                         'attractive_ad_form' =>
        //                         $attractive_ad_form->createView(),
        //                         'attractive_ad' => $attractive_ad,

        // ]);

        return $this->render("Dashboard/PaidOptions/PaidOptions.html.twig");
    }

    // /**
    // * @Route("/edit/{id}",name="edit_paidoptions")
    // * @Method({"POST"})
    // */
    // public function editAccountAction(Request $request,PaidOptions $paidoptions)
    // {
    //     $form = $this->createCreateForm($paidoptions);
    //     $form->handleRequest($request);
    //     if($form->isSubmitted() && $form->isValid())
    //     {
    //         $this->getDoctrine()->getManager()->flush();
    //         $this->addFlash('success','Operation a été passé avec succés');
    //     }
    // return $this->redirectToRoute('paidoptions_list_index');
    // }

    /**
     * @Route("/packs/{ptype}",name="paidoptions_new_pack")
     * @Method({"GET", "POST"})
     */
    public function NewPackAction(Request $request, $ptype)
    {
        $Packs;
        $em          = $this->getDoctrine()->getManager();
        $paidoptions = new PaidOptions();
        $form        = $this->createForm('AppBundle\Form\PaidOptionsType', $paidoptions);
        $form->handleRequest($request);

        if ($ptype == PaidOptions::TYPE_ALWAYS_IN_LEAD) {
            $Packs = $em->getRepository('AppBundle:PaidOptions')->findBy(['type' => PaidOptions::TYPE_ALWAYS_IN_LEAD]);
        }

        if ($ptype == PaidOptions::TYPE_RENEWAL) {
            $Packs = $em->getRepository('AppBundle:PaidOptions')->findBy(['type' => PaidOptions::TYPE_RENEWAL]);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            if ($ptype == PaidOptions::TYPE_ALWAYS_IN_LEAD) {
                $paidoptions->setType(PaidOptions::TYPE_ALWAYS_IN_LEAD);
            }

            if ($ptype == PaidOptions::TYPE_RENEWAL) {
                $paidoptions->setType(PaidOptions::TYPE_RENEWAL);
            }
            $em->persist($paidoptions);
            $em->flush();
            $this->addFlash('success', 'nouveau Pack a été ajouté avec succés');
            return $this->redirectToRoute('paidoptions_new_pack', array('ptype' => $ptype));
        }

        return $this->render("Dashboard/PaidOptions/ListPaidOptionsNew.html.twig", [
            'packs' => $Packs,
            'form'  => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing paidOptions entity.
     *
     * @Route("/{id}/edit", name="pack_edit")
     * @Method({"GET", "POST"})
     */
    public function editPackAction(Request $request, PaidOptions $paidoptions)
    {

        $em   = $this->getDoctrine()->getManager();
        $form = $this->createForm('AppBundle\Form\PaidOptionsType', $paidoptions);
        $form->handlerequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($paidoptions);
            $em->flush();
            $this->addFlash('success', 'le Pack a été modifie avec succés');
            return $this->redirectToRoute('paidoptions_new_pack', ['ptype' => $paidoptions->getType()]);
        }

        return $this->render('Dashboard/PaidOptions/EditPaidOptions.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    protected function createCreateForm(PaidOptions $paidoptions)
    {
        $form = $this->createForm('AppBundle\Form\PaidOptionsType', $paidoptions);
        return $form->createView();
    }

    /**
     * @Route("/delete/{id}", options={ "expose" = true },
     * condition="request.isXmlHttpRequest()",name="pack_delete")
     */
    public function deleteAction(Request $request, PaidOptions $paidoption)
    {
        if ($paidoption) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($paidoption);
            $em->flush();
            return new JsonResponse(['success' => true]);
        } else {
            return new JsonResponse(['success' => false]);
        }
    }

}
