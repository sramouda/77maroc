<?php
namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\Ad;
use AppBundle\Entity\BoxAds;
use AppBundle\Entity\User;
use AppBundle\Entity\MessageType;
use AppBundle\Services\AutoDetectedBoiteAnnonce;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/boxads")
 */
class BoxAdsController extends Controller
{
    /**
     * @Route("/",name="boiteAnnonce_list")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_BOITES_ANNONCES') ")
     */
    public function indexAction(Request $request)
    {
        $em           = $this->getDoctrine()->getManager();
        $boxAds       = $em->getRepository('AppBundle:BoxAds')->findAll();
        $AdminsActive = $em->getRepository('AppBundle:User')->findUsersByRole('ROLE_ADMIN', true);
        return $this->render('Dashboard/BoxAds/BoxAds.html.twig', [
            'BoxAds' => $boxAds,
            'admins' => $AdminsActive,
        ]);
    }
    /**
     * @Route("/{admin_id}/affecter/{boite_id}", options={ "expose" = true },
     * condition="request.isXmlHttpRequest()",name="boiteAnnonce_affecter_admin")
     * @ParamConverter("user", options={"mapping": {"admin_id": "id"}})
     * @ParamConverter("box", options={"mapping": {"boite_id": "id"}})
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_BOITES_ANNONCES')")
     */
    public function AffecterAction(Request $request, User $user, BoxAds $box)
    {
        if ($user && $box) {
            $em = $this->getDoctrine()->getManager();
            $box->setUser($user);
            $em->flush();
            return new JsonResponse(['success' => true]);
        } else {
            return new JsonResponse(['success' => false]);
        }

    }

    /**
     * @Route("/activeted/{id}", options={ "expose" = true },
     * condition="request.isXmlHttpRequest()",name="boiteAnnonce_activeted")
     * @ParamConverter("user", options={"mapping": {"id": "id"}})
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_BOITES_ANNONCES')")
     */
    public function ActivetedAction(Request $request, BoxAds $box)
    {
        $em                       = $this->getDoctrine()->getManager();
        $AutoDetectedBoiteAnnonce = $this->get(AutoDetectedBoiteAnnonce::class);
        $check                    = $request->request->get('check');
        if ($box) {
            if (!$check) {
                $ads = $em->getRepository(Ad::class)->findBy(['boxad' => $box->getId(), 'status' => null]);
                foreach ($ads as $ad) {
                    $ad->setBoxad($AutoDetectedBoiteAnnonce->detectedBoite(true, $box->getId()));
                    $em->flush();
                }

            }
            $box->setBoitecheck($check);
            $em->flush($box);
            return new JsonResponse(['success' => true]);
        } else {
            return new JsonResponse(['success' => false]);
        }

    }
    /**
     * @Route("/box/{id}",name="box_ad_index")
     */
    public function indexBoiteAction(Request $request, BoxAds $box)
    {
        $em              = $this->getDoctrine()->getManager();
        $ads             = $em->getRepository(Ad::class)->findBy(['boxad' => $box->getId(), 'status' => null], array('id' => 'ASC'));
        $boiteDesactiver = $box->getBoitecheck() ? false : true;

        return $this->render('Dashboard/BoxAds/box.html.twig', ['ads' => $ads,
                                                                'boiteDesactiver'   => $boiteDesactiver,
        ]);

    }

    public function getTemplateMessageAction()
    {
         $em               = $this->getDoctrine()->getManager();
         $messageTemplates = $em->getRepository(MessageType::class)->findAll();
         return $this->render('Partials/templatesMessage.html.twig',['messageTemplates' => $messageTemplates]);
    }
 
    /**
    * @Route("/box/ad/{id}/details/",name="box_ad_trait")
    */
    public function checkTraitAdAction(Request $request,Ad $ad)
    {
        return $this->render('Dashboard/BoxAds/ad_box_trait.html.twig',['ad' => $ad]);

    }
     /**
     * @Route("/box/ad/trait/{id}",name="box_ad_traitement")
     * @Security("has_role('ROLE_NORMAL') or has_role('ROLE_VETRINE') or has_role('ROLE_SUPER_ADMIN')")
     */
    public function dipostAdtraitAction(Request $request,Ad $ad)
    {
         $em = $this->getDoctrine()->getManager();
         $inbox = new Inbox();
         $inbox->setUser($ad->getUser());
         $inbox->setType(Inbox::SYSMESSGAE);
        
        if($request->request->get('ad-operation') == 'Refuser')
        {
             $ad->setStatus(false);
             $message = $em->getRepository(MessageTemplate::class)->findOneBy(['id' => $request->request->get('messageRefuser')]);
            $inbox->setSubject('Refuser votre annonce : '.$ad->getTitle());
            $inbox->setText($message->getTemplate());
            $em->persist($inbox); 
        }else{
            $idsFiles = $request->request->get('deleteFile');
            if(!empty($idsFiles))
            {
                foreach ($idsFiles as $key => $value) {
                    $image = $em->getRepository(ImagesAd::class)->findOneBy(['id' => $key]);
                    $ad->removeImage($image);
                    $this->get('vich_uploader.upload_handler')->remove($image, 'pathFile');
                }
            }
            $ad->setListoptions($request->request->get('critiriaOption'));
            $ad->setStatus(true);
            $ad->setPrice($request->request->get('price'));
            $ad->setCategory($request->request->get('categories'));
            $ad->setTitle($request->request->get('title'));
            $ad->setDescription($request->request->get('description'));
            $ad->SetTypeannonce('Offre');
            $ad->setUrlwebsite($request->request->get('urlwebsite'));
            $ad->setUrlyoutube($request->request->get('urlyoutube')); 
            $ad->setLatlng($request->request->get('latLng'));
            $ad->setVillename($request->request->get('ville'));
            $ad->setSecteurname($request->request->get('region'));
            $message = $em->getRepository(MessageType::class)->findOneBy(['type' => MessageType::ANNONCE_VALIDER]);
            $inbox->setSubject('Accepter votre annonce : '.$ad->getTitle());
            $inbox->setText($message->getMessagetemplate()[0]->getTemplate());
            $em->persist($inbox); 
        }
        $em->flush();
        $this->addFlash('success', 'Votre operation a été passé avec success');
        return $this->redirect($this->generateUrl('box_ad_index', array('id' => $ad->getBoxad())));
        
    }



}
