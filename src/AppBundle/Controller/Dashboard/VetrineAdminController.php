<?php

namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/vetrine")
 */

class VetrineAdminController extends Controller
{

    /**
     * @Route("/list",name="users_vetrine_list")
     * @Security("has_role('ROLE_SUPER_ADMIN') or  has_role('PERMISSION_USERS_VETRINE_ACTIVE')")
     */
    public function listUsersVetrineAction(Request $request)
    {

        $em           = $this->getDoctrine()->getManager();
        $usersVetrine = $em->getRepository('AppBundle:User')->findUsersByRole("ROLE_VETRINE");
        return $this->render('Dashboard/Vetrine/user.html.twig', [
            'users' => $usersVetrine,
        ]);
    }

    /**
     * @Route("/add",name="users_vetrine_add")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_VETRINE_ACTIVE')")
     * @Method({"GET", "POST"})
     */
    public function addVetrineAction(Request $request)
    {

        $userManager    = $this->container->get('fos_user.user_manager');
        $user           = $userManager->createUser();
        $formCreateUser = $this->createForm('AppBundle\Form\UserType', $user, ['normalOrVetrine' => true, 'isVetrine' => true]);
        $formCreateUser->handleRequest($request);

        if ($formCreateUser->isSubmitted() && $formCreateUser->isValid()) {
            $user->setUsername($user->getEmail());
            $user->setRoles(["ROLE_VETRINE"]);
            $userManager->updateUser($user);
            $this->addFlash('success', 'Noveau utilisateur Vetrine a été ajouté avec succés');
            return $this->redirectToRoute('users_vetrine_list');
        }

        return $this->render('Dashboard/Vetrine/AddUser.html.twig', array(
            'user'           => $user,
            'formCreateUser' => $formCreateUser->createView(),
        ));

    }

    /**
     * @Route("/{id}/edit", name="users_vetrine_edit")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_VETRINE_ACTIVE')")
     * @Method({"GET", "POST"})
     */
    public function editVetrineAction(Request $request, User $user)
    {
        $userManager  = $this->container->get('fos_user.user_manager');
        $formEditUser = $this->createForm('AppBundle\Form\UserType', $user, ['normalOrVetrine' => true,
            'required'                                                                             => false, 'isVetrine' => true]);
        $formEditUser->handleRequest($request);

        if ($formEditUser->isSubmitted() && $formEditUser->isValid()) {
            $userManager->updateUser($user);
            $this->addFlash('success', 'La modification a été passé avec succés');
            return $this->redirectToRoute('users_vetrine_list');
        }

        return $this->render('Dashboard/Vetrine/EditUser.html.twig', array(
            'user'         => $user,
            'formEditUser' => $formEditUser->createView(),
        ));

    }

}
