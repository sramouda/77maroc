<?php

namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/normal")
 */

class NormalAdminController extends Controller
{

    /**
     * @Route("/list",name="users_noraml_list")
     * @Security("has_role('ROLE_SUPER_ADMIN') or  has_role('PERMISSION_USERS_NORMAL')")
     */
    public function listUsersNormalAction(Request $request)
    {

        $em          = $this->getDoctrine()->getManager();
        $usersNormal = $em->getRepository('AppBundle:User')->findUsersByRole("ROLE_NORMAL");
        return $this->render('Dashboard/Normal/user.html.twig', [
            'users' => $usersNormal,
        ]);
    }

    /**
     * @Route("/add",name="users_normal_add")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_NORMAL')")
     * @Method({"GET", "POST"})
     */
    public function addNormalAction(Request $request)
    {

        $userManager    = $this->container->get('fos_user.user_manager');
        $user           = $userManager->createUser();
        $formCreateUser = $this->createForm('AppBundle\Form\UserType', $user, ['normalOrVetrine' => true]);
        $formCreateUser->handleRequest($request);

        if ($formCreateUser->isSubmitted() && $formCreateUser->isValid()) {
            $user->setUsername($user->getEmail());
            $user->setRoles(["ROLE_NORMAL"]);
            $userManager->updateUser($user);
            $this->addFlash('success', 'Noveau utilisateur normale a été ajouté avec succés');
            return $this->redirectToRoute('users_noraml_list');
        }

        return $this->render('Dashboard/Normal/AddUser.html.twig', array(
            'user'           => $user,
            'formCreateUser' => $formCreateUser->createView(),
        ));

    }

    /**
     * @Route("/{id}/edit", name="users_normal_edit")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_NORMAL')")
     * @Method({"GET", "POST"})
     */
    public function editNormalAction(Request $request, User $user)
    {
        $userManager  = $this->container->get('fos_user.user_manager');
        $formEditUser = $this->createForm('AppBundle\Form\UserType', $user, ['normalOrVetrine' => true,
            'required'                                                                             => false]);
        $formEditUser->handleRequest($request);

        if ($formEditUser->isSubmitted() && $formEditUser->isValid()) {
            $userManager->updateUser($user);
            $this->addFlash('success', 'La modification a été passé avec succés');
            return $this->redirectToRoute('users_noraml_list');
        }

        return $this->render('Dashboard/Normal/EditUser.html.twig', array(
            'user'         => $user,
            'formEditUser' => $formEditUser->createView(),
        ));

    }

}
