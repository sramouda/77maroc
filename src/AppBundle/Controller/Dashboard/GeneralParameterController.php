<?php

namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\GeneralParameter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/generalparameter")
 */
class GeneralParameterController extends Controller
{
    /**
     * @Route("/",name="general_parameter_index")
     */
    public function indexAction(Request $request)
    {
        $em                     = $this->getDoctrine()->getManager();
        $number_of_ads_per_page = $em->getRepository('AppBundle:GeneralParameter')->findOneBy(['name' => 'NUMBER_OF_ADS_PER_PAGE']);

        $number_of_showcases_per_page   = $em->getRepository('AppBundle:GeneralParameter')->findOneBy(['name' => 'NUMBER_OF_SHOWCASES_PER_PAGE']);
        $number_of_premium_ads_per_page = $em->getRepository('AppBundle:GeneralParameter')->findOneBy(['name' => 'NUMBER_OF_PREMIUM_ADS_PER_PAGE']);
        $always_on_top_per_page         = $em->getRepository('AppBundle:GeneralParameter')->findOneBy(['name' => 'ALWAYS_ON_TOP_PER_PAGE']);
        $currency                       = $em->getRepository('AppBundle:GeneralParameter')->findOneBy(['name' => 'CURRENCY']);

        $ads_per_page_form = $this->createCreateForm($number_of_ads_per_page);
        // dump($ads_per_page_form);
        // die();
        $showcases_per_page_form   = $this->createCreateForm($number_of_showcases_per_page);
        $premium_ads_per_page_form = $this->createCreateForm($number_of_premium_ads_per_page);
        $top_per_page_form         = $this->createCreateForm($always_on_top_per_page);
        $currency_form             = $this->createCreateForm($currency);

        return $this->render("Dashboard/GeneralParameter/GeneralParameter.html.twig", [
            'ads_per_page_form'              => $ads_per_page_form->createView(),

            'showcases_per_page_form'        => $showcases_per_page_form->createView(),

            'premium_ads_per_page_form'      => $premium_ads_per_page_form->createView(),

            'top_per_page_form'              => $top_per_page_form->createView(),

            'currency_form'                  => $currency_form->createView(),

            'number_of_ads_per_page'         => $number_of_ads_per_page,
            'number_of_showcases_per_page'   => $number_of_showcases_per_page,
            'number_of_premium_ads_per_page' => $number_of_premium_ads_per_page,
            'always_on_top_per_page'         => $always_on_top_per_page,
            'currency'                       => $currency,

        ]);

    }

    /**
     * @Route("/edit/{id}",name="edit_general_parameter")
     * @Method({"POST"})
     */
    public function editAccountAction(Request $request, GeneralParameter $generalParameter)
    {
        $form = $this->createCreateForm($generalParameter);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Operation a été passé avec succés');
        }
        return $this->redirectToRoute('general_parameter_index');
    }

    protected function createCreateForm(GeneralParameter $generalparameter)
    {
        $form = $this->createForm('AppBundle\Form\GeneralParameterType', $generalparameter);
        return $form;
    }

}
