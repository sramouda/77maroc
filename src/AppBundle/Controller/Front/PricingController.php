<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use AppBundle\Entity\Ad;
use AppBundle\Entity\PaidOptions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
* @Route("/pricing")
*/
class PricingController extends Controller
{
    /**
     * @Route("/renew/{ad}", name="renew_index")
     */
    public function renewAction(Request $request,Ad $ad)
    {
        $em = $this->getDoctrine()->getManager(); 
        $renewpack = $em->getRepository('AppBundle:PaidOptions')->findBy(['type' => 'RENEWAL']);
        return $this->render('Default/PricingModel/RenewPricingModel.html.twig',[
         'packs' => $renewpack,
         'ad' => $ad

        ]);
    }

    /**
     * @Route("/always_in_lead/{ad}", name="always_index")
     */
    public function alwaysAction(Request $request,Ad $ad)
    {
        $em = $this->getDoctrine()->getManager(); 
        $alwyspack = $em->getRepository('AppBundle:PaidOptions')->findBy(['type' => 'ALWAYS_IN_LEAD']);
        return $this->render('Default/PricingModel/AlwaysPricingModel.html.twig',[
         'packs' => $alwyspack,
         'ad' => $ad

        ]);
    }

    /**
     * @Route("/buypack/{id}/{ad}", name="buypack_index")
     */
    public function buypackAction(Request $request,PaidOptions $po,Ad $ad)
    {
        // dump($po);
        // die();
        $em = $this->getDoctrine()->getManager(); 
        
        return $this->render('Default/PricingModel/BuyPack.html.twig',[
         'pack' => $po,
         'ad' => $ad

        ]);
    }

     

    
}
