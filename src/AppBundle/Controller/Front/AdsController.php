<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\Ad;
use AppBundle\Entity\AdsCriteriasOption;
use AppBundle\Entity\Category;
use AppBundle\Entity\ImagesAd;
use AppBundle\Entity\Inbox;
use AppBundle\Services\AutoDetectedBoiteAnnonce;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/annonces")
 */
class AdsController extends Controller
{
    /**
     * @Route("/", name="ads_page")
     * @Route("/{slug}", name="ads_category_index")
     */
    public function indexAction(Request $request)
    {

        $em      = $this->getDoctrine()->getManager();
        $adsList = $em->getRepository('AppBundle:Ad')->findBy(['status' => true], ['createdAt' => 'DESC']);

        if ($request->attributes->get('slug')) {
            $category = $em->getRepository('AppBundle:Category')->findOneBy(["slug" => $request->attributes->get('slug')]);
            $cats     = $em->getRepository('AppBundle:Category')->findBy(["parent" => $category->getId()]);
            $adsList  = $em->getRepository('AppBundle:Ad')->findBy(["category" => $cats], ['createdAt' => 'DESC']);

        }

        $numberAllAds = count($adsList);

        $adsPar = array_filter($adsList, function ($ad) {
            return $ad->getType() == 'par';
        });
        $numberParAds = count($adsPar);


        $adsPro = array_filter($adsList, function ($ad) {
            return $ad->getType() == 'pro';
        });
        // $adsFeature = array_filter($adsList ,function($ad) {
        //        return $ad->getFeaturestemp() == 'yes';
        //     }) ;

        $numberProAds = count($adsPro);
        $type         = $request->query->get('type');

        if ($type) {
            $adsList = array_filter($adsList, function ($ad) use ($type) {
                return $ad->getType() == $type;
            });
        }

        $ConfigAdsOfPage = $em->getRepository('AppBundle:GeneralParameter')->findOneBy(['name' => 'NUMBER_OF_ADS_PER_PAGE']);
        $numberAdsOfPage = $ConfigAdsOfPage->getValue() ?: 10;

        $paginator = $this->get('knp_paginator');
        $ads       = $paginator->paginate(
            $adsList,
            $request->query->getInt('page', 1),
            intVal($numberAdsOfPage)
        );

        return $this->render('Default/Ads/Ads.html.twig', array(

            'ads'          => $ads,
            // 'adsFeature' => $adsFeature,
            'numberAllAds' => $numberAllAds,
            'numberParAds' => $numberParAds,
            'numberProAds' => $numberProAds,
        ));
    }

    /**
     *@Route("/details/{slug}",name="detail_ad")
     *@ParamConverter("ad", options={"mapping": {"slug": "slug"}})
     */
    public function detailAdIndex(Request $request, Ad $ad)
    {
         $em = $this->getDoctrine()->getManager();
        $ad->setNumbervisit($ad->getNumbervisit() + 1);
        $this->getDoctrine()->getManager()->flush();

          $inbox = new Inbox();

        $form = $this->createForm('AppBundle\Form\InboxType',$inbox);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $inbox->setType(Inbox::ANNONYME);
            $inbox->setSubject($ad->getTitle());
            $inbox->setUser($ad->getUser());
            $em->persist($inbox);
            $em->flush();
          return $this->redirectToRoute('detail_ad',['slug' => $ad->getSlug()]);
        }
        return $this->render('Default/Ads/adDetail.html.twig', [
            'ad' => $ad,
            "form" => $form->createView()

        ]);
    }

    /**
     *@Route("/deposer-annonce/", name="dipost_ad")
     */
    public function dipostAdAction(Request $request)
    {
        return $this->render('Default/Ads/depositAd.html.twig');
    }

   /**
    * @Route("/deposer-annonce/trait",name="dipost_ad_trait")
    * @Security("has_role('ROLE_NORMAL') or has_role('ROLE_VETRINE') or has_role('ROLE_SUPER_ADMIN')")
    */
   public function dipostAdtraitAction(Request $request)
   {
       $em                       = $this->getDoctrine()->getManager();
       $ad                       = new Ad();
       $AutoDetectedBoiteAnnonce = $this->get(AutoDetectedBoiteAnnonce::class);
       $ad->setUser($this->getUser());
       $ad->setBoxad($AutoDetectedBoiteAnnonce->detectedBoite());
       $ad->setPrice($request->request->get('price'));
         if($this->getUser()->hasRole('ROLE_VETRINE'))
           {
               $ad->setType('pro');
           }else{
               $ad->setType('par');
           }
       $ad->setListoptions($request->request->get('critiriaOption'));

        foreach ($request->files as $key => $file) {
           
                   $image = new ImagesAd();
                   $image->setPathFile($file);
                   $image->setAds($ad);
                   $image->setAlt($key);
                   $em->persist($image);
                   $ad->addImage($image);
        }

       $ad->setCategory($request->request->get('categories'));
       $ad->setTitle($request->request->get('title'));
       $ad->setDescription($request->request->get('description'));
       $ad->SetTypeannonce('Offre');
       $ad->setUrlwebsite($request->request->get('lienWebSite'));
       $ad->setUrlyoutube($request->request->get('lienYoutube'));

       $ad->setLatlng($request->request->get('latLng'));

       $ad->setVillename($request->request->get('ville'));
       $ad->setSecteurname($request->request->get('region'));

       $validator = $this->get('validator');
       $errors    = $validator->validate($ad);
       if (count($errors) > 0) {
           throw new NotFoundHttpException('Sorry not existing!');
       } else {
           $em->persist($ad);
           $em->flush();
           $this->addFlash('success', 'Votre annonce bien ajouter ,attendez la confiramation de notre equipe');
           return $this->redirectToRoute('dipost_ad');

       }

   }

}
