<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use AppBundle\Entity\Ad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
* @Route("/espace_normal")
* @Security("has_role('ROLE_NORMAL')")
*/
class NormalSpaceController extends Controller
{
    /**
     * @Route("/", name="espace_normal_index")
     */
    public function indexAction(Request $request)
    {
    	$usr= $this->getUser();
        // dump($usr);
        // die();

        return $this->render('Default/Spaces/Normal/Solde.html.twig', [
              'user' => $usr,
        ]);
    }


     /**
     * @Route("/mes_infos", name="mes_info_index")
     */
    public function infosAction(Request $request)
    {
    	$usr= $this->getUser();
        $form = $this->createForm('AppBundle\Form\UserRegistrationType',$usr);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->getDoctrine()->getManager()->flush();
            $this->redirectToRoute('mes_info_index');
        }

        return $this->render('Default/Spaces/Normal/Infos.html.twig', [
              'user' => $usr,
              'form' => $form->createView()
        ]);
    }
    
    /**
    * @Route("/liste_annonces/{type}", name="annonces_liste_index")
    */
    public function enabledAdsAction(Request $request,$type){
         

         $em = $this->getDoctrine()->getManager();
         $usr = $this->getUser();

         $numberOfAdsAllowed = $usr->getAccounttype()->getNumberofactiveads();
         if($type == "actives"){
               $lads = $em->getRepository('AppBundle:Ad')->findBy(['user' => $usr,'status' => true]);
         }else if($type == "inactives"){
               $lads = $em->getRepository('AppBundle:Ad')->findBy(['user' => $usr,'status' => false]);
         }else{
               $lads = $em->getRepository('AppBundle:Ad')->findBy(['user' => $usr,'status' => null]);
         }
         $numberofads = count($lads);
         $paginator  = $this->get('knp_paginator');
         $ads = $paginator->paginate(
                    $lads,
                    $request->query->getInt('page',1),
                    intVal(5)
         );
      
         return $this->render('Default/Spaces/Normal/ListAds.html.twig', [
              
              'numberOfAdsAllowed' => $numberOfAdsAllowed,
              'ads' => $ads,
              'type' => $type,
              'numberofads' => $numberofads
         ]);

    }
    
    /**
    * @Route("/disablead/{id}", name="disable_ad")
    */
    public function disableAdAction(Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();
        if($ad){
             $ad->setStatus(false);
             $em->flush();
             return $this->redirectToRoute('annonces_liste_index', ['type' => 'inactives']);
        }
    }

 
    /**
     * @Route("/recharger_compte", name="sold_payment_index")
     */
    public function SoldPaymentAction(Request $request)
    {
     
      
        return $this->render('Default/Spaces/Normal/SoldePayment.html.twig');
    }

    /**
     * @Route("/inbox", name="inbox_normal_index")
     */
    public function inboxAction(Request $request)
    {
     
        $usr = $this->getUser();
        $inbox = $usr->getInbox();
        return $this->render('Default/Spaces/Normal/inbox.html.twig', [
            
            'inbox' => $inbox
        ]);
    }

    
}
