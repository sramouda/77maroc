<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 */

class UserController extends Controller
{
    /**
     * @Route("/account/registration",name="user_registration")
     * @Method({"GET", "POST"})
     */
    public function RegisterAction(Request $request)
    {

        $userManager = $this->container->get('fos_user.user_manager');
        $user        = $userManager->createUser();

        $formCreateUser = $this->createForm('AppBundle\Form\UserRegistrationType', $user);
        $formCreateUser->handleRequest($request);

        if ($formCreateUser->isSubmitted() && $formCreateUser->isValid()) {
            $user->setUsername($user->getEmail());
            $user->setRoles(["ROLE_NORMAL"]);
            $userManager->updateUser($user);
            return $this->redirectToRoute('fos_user_security_login');
        }

        return $this->render('Default/User/Register.html.twig', array(
            'form' => $formCreateUser->createView(),
        ));

    }
}
