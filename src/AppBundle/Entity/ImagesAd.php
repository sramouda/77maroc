<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ImagesAd
 *
 * @ORM\Table(name="`7m_images_ad`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImagesAdRepository")
 * @Vich\Uploadable
 */
class ImagesAd
{
   
   /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Ad",inversedBy="images")
     * @ORM\JoinColumn(name="ads_id", referencedColumnName="id",nullable=true)
     */
     private $ads;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */

     private $path;

    /**
     * @Vich\UploadableField(mapping="images_ads", fileNameProperty="path")
     *
     * @var File
     */
     private $pathFile;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;
    /**
     * @var string
     *
     * @ORM\Column(name="Alt", type="string", length=120, nullable=true)
     */
    private $alt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return images
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set alt
     *
     * @param string $alt
     * @return images
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set images
     *
     * @param \AppBundle\Entity\ImagesAd  $images
     * @return images
     */
    public function setImages(\AppBundle\Entity\ImagesAd $images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return \AppBundle\Entity\ImagesAd 
     */
    public function getImages()
    {
        return $this->images;
    }


    /**
     * Set ads
     *
     * @param \Ad $ads
     * @return images
     */
    public function setAds(\AppBundle\Entity\Ad $ads = null)
    {
        $this->ads = $ads;

        return $this;
    }

    /**
     * Get ads
     *
     * @return \Ad
     */
    public function getAds()
    {
        return $this->ads;
    }
    /**
     * @param array $path
     */
    public function setPathFile($path = null)
    {
        if (is_array($path) && $path) {
            $path = $path[0];
        }
        if ($path instanceof File) {
            $this->pathFile = $path;
            $this->updatedAt = new \Datetime();
        }
    }

    /**
     * @return File
     */
    public function getPathFile()
    {
        return $this->pathFile;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return images
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    public function __toString()
    {
        return 'Images';
    }
}
