<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ad
 *
 * @ORM\Table(name="7m_ad")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdRepository")
 */
class Ad
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)

     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)

     */
    private $description;


     /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
     private $type;
        /**
     * @var string
     *
     * @ORM\Column(name="typeannonce", type="string", length=50)
     */
     private $typeannonce;


     /**
     * @ORM\Column(name="slug", type="string", length=255)
     * @Gedmo\Slug(fields={"title", "id"})
     */
     private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean",nullable=true)
     */
    private $status;

  

    /** 
    * @ORM\OneToMany(targetEntity="AdsPaidOptions", mappedBy="ad",fetch="EAGER")
    */
    private $adsPaidOptions;
    
    /**
    * Many Ads to one User
    *@ORM\ManyToOne(targetEntity="User", inversedBy="Ad")
    *@ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    private $user;



    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var string
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="villename", type="string",length=255)

     */
    private $villename;
     /**
     * @var string
     *
     * @ORM\Column(name="secteurname", type="string",length=255)

     */
     private $secteurname;
       /**
     * @var string
     *
     * @ORM\Column(name="category", type="string",length=255)

     */
      private $category;
     /**
     * @var float
     *
     * @ORM\Column(name="price", type="float",nullable=true)
     */
     private $price;
     /**
     * @ORM\OneToMany(targetEntity="ImagesAd", mappedBy="ads", cascade={"persist"}, orphanRemoval=true,fetch="EAGER")
     * @ORM\OrderBy({"id" = "ASC"})
     */
     private $images;
    
    /**
    *@ORM\Column(name="number_visit",type="integer",nullable=true)
    */
    private $numbervisit;
        /**
     * @var array
     *
     * @ORM\Column(name="listoptions", type="array")
     */
    private $listoptions;
    
    /**
     * @ORM\Column(name="box_ad",type="integer")
    */
    private $boxad;


    /**
     * @var string
     *
     * @ORM\Column(name="urlwebsite", type="string", length=255)
  
     */
    private $urlwebsite;
    /**
     * @var string
     *
     * @ORM\Column(name="urlyoutube", type="string", length=255)
     */
    private $urlyoutube; 
     /**
     * @var string
     *
     * @ORM\Column(name="latlng", type="string", length=255)
     */
     private $latlng;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Ad
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getNumbervisit()
    {
        return $this->numbervisit;
    }
    public function setNumbervisit($numbervisit)
    {
        $this->numbervisit = $numbervisit;
        return $this;
    }
 
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Ad
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Ad
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Ad
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

   

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Ad
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Ad
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    public function setType($type){
        $this->type = $type;
        return $this;

    }
    public function getType(){
        return $this->type;
    }

  

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Ad
     */
    public function setSlug($slug)
    {
        $this->slug =$this->getId().'-'.$slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }



    /**
     * Add adsPaidOption
     *
     * @param \AppBundle\Entity\AdsPaidOptions $adsPaidOption
     *
     * @return Ad
     */
    public function addAdsPaidOption(\AppBundle\Entity\AdsPaidOptions $adsPaidOption)
    {
        $this->adsPaidOptions[] = $adsPaidOption;

        return $this;
    }

    /**
     * Remove adsPaidOption
     *
     * @param \AppBundle\Entity\AdsPaidOptions $adsPaidOption
     */
    public function removeAdsPaidOption(\AppBundle\Entity\AdsPaidOptions $adsPaidOption)
    {
        $this->adsPaidOptions->removeElement($adsPaidOption);
    }

    /**
     * Get adsPaidOptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdsPaidOptions()
    {
        return $this->adsPaidOptions;
    }

 

    /**
     * Add image
     *
     * @param \AppBundle\Entity\ImagesAd $image
     *
     * @return Ad
     */
    public function addImage(\AppBundle\Entity\ImagesAd $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \AppBundle\Entity\ImagesAd $image
     */
    public function removeImage(\AppBundle\Entity\ImagesAd $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Ad
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set typeannonce
     *
     * @param string $typeannonce
     *
     * @return Ad
     */
    public function setTypeannonce($typeannonce)
    {
        $this->typeannonce = $typeannonce;

        return $this;
    }

    /**
     * Get typeannonce
     *
     * @return string
     */
    public function getTypeannonce()
    {
        return $this->typeannonce;
    }



    /**
     * Set villename
     *
     * @param string $villename
     *
     * @return Ad
     */
    public function setVillename($villename)
    {
        $this->villename = $villename;

        return $this;
    }

    /**
     * Get villename
     *
     * @return string
     */
    public function getVillename()
    {
        return $this->villename;
    }

 

    /**
     * Set secteurname
     *
     * @param string $secteurname
     *
     * @return Ad
     */
    public function setSecteurname($secteurname)
    {
        $this->secteurname = $secteurname;

        return $this;
    }

    /**
     * Get secteurname
     *
     * @return string
     */
    public function getSecteurname()
    {
        return $this->secteurname;
    }

  
    /**
     * Set boxad
     *
     * @param integer $boxad
     *
     * @return Ad
     */
    public function setBoxad($boxad)
    {
        $this->boxad = $boxad;

        return $this;
    }

    /**
     * Get boxad
     *
     * @return integer
     */
    public function getBoxad()
    {
        return $this->boxad;
    }

    /**
     * Set listoptions
     *
     * @param array $listoptions
     *
     * @return Ad
     */
    public function setListoptions($listoptions)
    {
        $this->listoptions = $listoptions;

        return $this;
    }

    /**
     * Get listoptions
     *
     * @return array
     */
    public function getListoptions()
    {
        return $this->listoptions;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Ad
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set urlwebsite
     *
     * @param string $urlwebsite
     *
     * @return Ad
     */
    public function setUrlwebsite($urlwebsite)
    {
        $this->urlwebsite = $urlwebsite;

        return $this;
    }

    /**
     * Get urlwebsite
     *
     * @return string
     */
    public function getUrlwebsite()
    {
        return $this->urlwebsite;
    }

    /**
     * Set urlyoutube
     *
     * @param string $urlyoutube
     *
     * @return Ad
     */
    public function setUrlyoutube($urlyoutube)
    {
        $this->urlyoutube = $urlyoutube;

        return $this;
    }

    /**
     * Get urlyoutube
     *
     * @return string
     */
    public function getUrlyoutube()
    {
        return $this->urlyoutube;
    }

    /**
     * Set latlng
     *
     * @param string $latlng
     *
     * @return Ad
     */
    public function setLatlng($latlng)
    {
        $this->latlng = $latlng;

        return $this;
    }

    /**
     * Get latlng
     *
     * @return string
     */
    public function getLatlng()
    {
        return $this->latlng;
    }
}