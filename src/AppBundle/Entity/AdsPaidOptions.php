<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AdsPaidOptions
 *
 * @ORM\Table(name="7m_ads_paid_options")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdsPaidOptionsRepository")
 */
class AdsPaidOptions
{
 
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;   

    /** 
     * @ORM\ManyToOne(targetEntity="Ad", inversedBy="adsPaidOptions") 
     * @ORM\JoinColumn(name="ad_id", referencedColumnName="id", nullable=false) 
     */
    private $ad;

    /** 
     * @ORM\ManyToOne(targetEntity="PaidOptions", inversedBy="adsPaidOptions") 
     * @ORM\JoinColumn(name="paidoptions_id", referencedColumnName="id", nullable=false) 
     */
    private $paidOptions;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var string
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt;
    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return AdsPaidOptions
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return AdsPaidOptions
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return AdsPaidOptions
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return AdsPaidOptions
     */
    public function setAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad = $ad;

        return $this;
    }

    /**
     * Get ad
     *
     * @return \AppBundle\Entity\Ad
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set paidOptions
     *
     * @param \AppBundle\Entity\PaidOptions $paidOptions
     *
     * @return AdsPaidOptions
     */
    public function setPaidOptions(\AppBundle\Entity\PaidOptions $paidOptions)
    {
        $this->paidOptions = $paidOptions;

        return $this;
    }

    /**
     * Get paidOptions
     *
     * @return \AppBundle\Entity\PaidOptions
     */
    public function getPaidOptions()
    {
        return $this->paidOptions;
    }
}
