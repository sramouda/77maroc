<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * User
 *
 * @ORM\Table(name="`7m_user`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{ 
    const PERMISSION_BOX_ADS                = "Boites   Annonces";
    const PERMISSION_ACCOUNT_TYPE           = "Types    Compte";
    const PERMISSION_USERS_NORMAL           = "Comptes  Normale";
    const PERMISSION_USERS_VETRINE_ACTIVE   = "Vetrines Actives";
    const PERMISSION_USERS_VETRINE_EXPIRED  = "Vetrines Expirés";
    const PERMISSION_USERS_REQUEST_VETRINE  = "Demandes Vetrine";
  


    const ROLE_ADMIN_PARAMETER_GENEREUX     = "Parametre generale";

    const PERMISSION_BOITE_ONE              = "Boite annonce 1";
    const PERMISSION_BOITE_TWO              = "Boite annonce 2";
    const PERMISSION_BOITE_THREE            = "Boite annonce 3";
    const PERMISSION_BOITE_FOUR             = "Boite annonce 4";
    const PERMISSION_BOITE_FIVE             = "Boite annonce 5";
    const PERMISSION_BOITE_SIX              = "Boite annonce 6";


    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        $this->ad = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
    * @ORM\OneToMany(targetEntity="BoxAds",mappedBy="user")
    */
    private $boxads;
    /**
    * @ORM\ManyToOne(targetEntity="AccountType",inversedBy="user")
    * @ORM\JoinColumn(name="id_acounttype",referencedColumnName="id",nullable=true)
    */
    private $accounttype;


    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=50,nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=50,nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50,nullable=true)
     */
    private $phone;

    /**
     * @var bool
     *
     * @ORM\Column(name="isactive", type="boolean",nullable=true)
     */
    private $isactive;

    /**
     * @var float
     *
     * @ORM\Column(name="solde", type="float",nullable=true)
     */
    private $solde;
     /**
     * @var string
     *
     * @ORM\Column(name="`namevetrine`", type="string", length=255,nullable=true)
     */
    private $namevetrine;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=true)
     * @Gedmo\Slug(fields={"namevetrine"})
     */
     private $slug;

    /**
    * One User to many Ads
    * @ORM\OneToMany(targetEntity="Ad" , mappedBy="user",fetch="EAGER")
    */
     private $ad;

    /**
     * @ORM\OneToMany(targetEntity="Inbox", mappedBy="user",fetch="EAGER")  
     */
     private $inbox;

     /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"},nullable=true)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"},nullable=true)
     */
    private $updatedAt;

     

    public static function getConstPermissions()
    {
     return [
            self::PERMISSION_BOX_ADS                =>   "PERMISSION_BOX_ADS",
            self::PERMISSION_ACCOUNT_TYPE           =>   "PERMISSION_ACCOUNT_TYPE",
            self::PERMISSION_USERS_NORMAL           =>   "PERMISSION_USERS_NORMAL",
            self::PERMISSION_USERS_VETRINE_ACTIVE   =>   "PERMISSION_USERS_VETRINE_ACTIVE",
            self::PERMISSION_USERS_VETRINE_EXPIRED  =>   "PERMISSION_USERS_VETRINE_EXPIRED",
            self::PERMISSION_USERS_REQUEST_VETRINE  =>   "PERMISSION_USERS_REQUEST_VETRINE",
            self::ROLE_ADMIN_PARAMETER_GENEREUX     =>   "ROLE_ADMIN_PARAMETER_GENEREUX",
        ];
    }
  


    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set isactive
     *
     * @param boolean $isactive
     *
     * @return User
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return boolean
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set solde
     *
     * @param float $solde
     *
     * @return User
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;

        return $this;
    }

    /**
     * Get solde
     *
     * @return float
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * Add boxad
     *
     * @param \AppBundle\Entity\BoxAds $boxad
     *
     * @return User
     */
    public function addBoxad(\AppBundle\Entity\BoxAds $boxad)
    {
        $this->boxads[] = $boxad;

        return $this;
    }

    /**
     * Remove boxad
     *
     * @param \AppBundle\Entity\BoxAds $boxad
     */
    public function removeBoxad(\AppBundle\Entity\BoxAds $boxad)
    {
        $this->boxads->removeElement($boxad);
    }

    /**
     * Get boxads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBoxads()
    {
        return $this->boxads;
    }

    /**
     * Set accounttype
     *
     * @param \AppBundle\Entity\AccountType $accounttype
     *
     * @return User
     */
    public function setAccounttype(\AppBundle\Entity\AccountType $accounttype = null)
    {
        $this->accounttype = $accounttype;

        return $this;
    }

    /**
     * Get accounttype
     *
     * @return \AppBundle\Entity\AccountType
     */
    public function getAccounttype()
    {
        return $this->accounttype;
    }

    /**
     * Set namevetrine
     *
     * @param string $namevetrine
     *
     * @return User
     */
    public function setNamevetrine($namevetrine)
    {
        $this->namevetrine = $namevetrine;

        return $this;
    }

    /**
     * Get namevetrine
     *
     * @return string
     */
    public function getNamevetrine()
    {
        return $this->namevetrine;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return User
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return User
     */
    public function addAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad[] = $ad;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \AppBundle\Entity\Ad $ad
     */
    public function removeAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad->removeElement($ad);
    }

    /**
     * Get ad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add inbox
     *
     * @param \AppBundle\Entity\Inbox $inbox
     *
     * @return User
     */
    public function addInbox(\AppBundle\Entity\Inbox $inbox)
    {
        $this->inbox[] = $inbox;

        return $this;
    }

    /**
     * Remove inbox
     *
     * @param \AppBundle\Entity\Inbox $inbox
     */
    public function removeInbox(\AppBundle\Entity\Inbox $inbox)
    {
        $this->inbox->removeElement($inbox);
    }

    /**
     * Get inbox
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInbox()
    {
        return $this->inbox;
    }
}
