<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MessageType
 *
 * @ORM\Table(name="`7m_message_type`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageTypeRepository")
 */
class MessageType
{


    const ANNONCE_VALIDER   = "ANNONCE_VALIDER";
    const PAIMENT_OPTIONS   = "PAIMENT_OPTIONS"; 
    const ANNONCE_SUPPRIMER = "ANNONCE_SUPPRIMER";
    const ANNONCE_REFUSER   = "ANNONCE_REFUSER";


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    private $type;
     /**
     * @ORM\OneToMany(targetEntity="MessageTemplate" ,mappedBy="messagetype",fetch="EAGER")
     */
     private $messagetemplate;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return MessageType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messagetemplate = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add messagetemplate
     *
     * @param \AppBundle\Entity\MessageTemplate $messagetemplate
     *
     * @return MessageType
     */
    public function addMessagetemplate(\AppBundle\Entity\MessageTemplate $messagetemplate)
    {
        $this->messagetemplate[] = $messagetemplate;

        return $this;
    }

    /**
     * Remove messagetemplate
     *
     * @param \AppBundle\Entity\MessageTemplate $messagetemplate
     */
    public function removeMessagetemplate(\AppBundle\Entity\MessageTemplate $messagetemplate)
    {
        $this->messagetemplate->removeElement($messagetemplate);
    }

    /**
     * Get messagetemplate
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessagetemplate()
    {
        return $this->messagetemplate;
    }
}
