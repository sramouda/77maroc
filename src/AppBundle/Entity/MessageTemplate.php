<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MessageTemplate
 *
 * @ORM\Table(name="`7m_message_template`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageTemplateRepository")
 */
class MessageTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="template", type="text")
     */
    private $template;

    /**
    * @ORM\ManyToOne(targetEntity="MessageType",inversedBy="messagetemplate")
    * @ORM\JoinColumn(name="id_messagetype",referencedColumnName="id")
    */
    private $messagetype;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     */
    private $title;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return MessageTemplate
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return MessageTemplate
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set messagetype
     *
     * @param \AppBundle\Entity\MessageType $messagetype
     *
     * @return MessageTemplate
     */
    public function setMessagetype(\AppBundle\Entity\MessageType $messagetype = null)
    {
        $this->messagetype = $messagetype;

        return $this;
    }

    /**
     * Get messagetype
     *
     * @return \AppBundle\Entity\MessageType
     */
    public function getMessagetype()
    {
        return $this->messagetype;
    }
}
