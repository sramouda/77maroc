<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountType
 *
 * @ORM\Table(name="`7m_account_type`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountTypeRepository")
 */
class AccountType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    *@ORM\OneToMany(targetEntity="User",mappedBy="accounttype")
    */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="numberofphotos", type="integer")
     */
    private $numberofphotos;

    /**
     * @var int
     *
     * @ORM\Column(name="validityperiod", type="integer")
     */
    private $validityperiod;

    /**
     * @var int
     *
     * @ORM\Column(name="numberofactiveads", type="integer")
     */
    private $numberofactiveads;

    /**
     * @var string
     *
     * @ORM\Column(name="accountwording", type="string", length=50)
     */
    private $accountwording;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numberofphotos
     *
     * @param integer $numberofphotos
     *
     * @return AccountType
     */
    public function setNumberofphotos($numberofphotos)
    {
        $this->numberofphotos = $numberofphotos;

        return $this;
    }

    /**
     * Get numberofphotos
     *
     * @return int
     */
    public function getNumberofphotos()
    {
        return $this->numberofphotos;
    }

    /**
     * Set validityperiod
     *
     * @param integer $validityperiod
     *
     * @return AccountType
     */
    public function setValidityperiod($validityperiod)
    {
        $this->validityperiod = $validityperiod;

        return $this;
    }

    /**
     * Get validityperiod
     *
     * @return int
     */
    public function getValidityperiod()
    {
        return $this->validityperiod;
    }

    /**
     * Set numberofactiveads
     *
     * @param integer $numberofactiveads
     *
     * @return AccountType
     */
    public function setNumberofactiveads($numberofactiveads)
    {
        $this->numberofactiveads = $numberofactiveads;

        return $this;
    }

    /**
     * Get numberofactiveads
     *
     * @return int
     */
    public function getNumberofactiveads()
    {
        return $this->numberofactiveads;
    }

    /**
     * Set accountwording
     *
     * @param string $accountwording
     *
     * @return AccountType
     */
    public function setAccountwording($accountwording)
    {
        $this->accountwording = $accountwording;

        return $this;
    }

    /**
     * Get accountwording
     *
     * @return string
     */
    public function getAccountwording()
    {
        return $this->accountwording;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return AccountType
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
