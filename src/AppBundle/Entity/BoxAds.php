<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * BoiteAnnonces
 *
 * @ORM\Table(name="`7m_box_ads`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BoxAdsRepository")
 */
class BoxAds
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="User",inversedBy="boxads")
    * @ORM\JoinColumn(name="id_user",referencedColumnName="id",nullable=true)
    */
     private $user;
    /**
     * @var integer
     *
     * @ORM\Column(name="boite", type="integer")
     */
    private $boite;

    /**
     * @var bool
     *
     * @ORM\Column(name="boitecheck", type="boolean")
     */
    private $boitecheck;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var string
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set boite
     *
     * @param integer $boite
     *
     * @return BoxAds
     */
    public function setBoite($boite)
    {
        $this->boite = $boite;

        return $this;
    }

    /**
     * Get boite
     *
     * @return integer
     */
    public function getBoite()
    {
        return $this->boite;
    }

    /**
     * Set boitecheck
     *
     * @param boolean $boitecheck
     *
     * @return BoxAds
     */
    public function setBoitecheck($boitecheck)
    {
        $this->boitecheck = $boitecheck;

        return $this;
    }

    /**
     * Get boitecheck
     *
     * @return boolean
     */
    public function getBoitecheck()
    {
        return $this->boitecheck;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return BoxAds
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return BoxAds
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return BoxAds
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
