<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PublicityPosition
 *
 * @ORM\Table(name="`7m_publicity_position`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PublicityPositionRepository")
 * @Vich\Uploadable
 */


class PublicityPosition
{

    const TYPE_PUB_SCRIPT      = "PUB SCRIPT";
    const TYPE_PUB_IMAGE_URL   = "PUB IMAGES ET URL";

    const PAGE_PRINCIPALE      = "PAGE PRINCIPALE";
    const PAGE_ALL_ANNONCES    = "PAGE TOUT ANNONCES";
    const PAGE_ANNONCE         = "PAGE ANNONCE";

    const PAGE_PRINCIPALE_UNDER_RECHERCHE   = "PAGE_PRINCIPALE_UNDER_RECHERCHE";
    const PAGE_PRINCIPALE_UNDER_ANNONCES    = "PAGE_PRINCIPALE_UNDER_ANNONCES";

    const PAGE_ALL_ANNONCES_UNDER_RECHERCHE = "PAGE_ALL_ANNONCES_UNDER_RECHERCHE";
    const PAGE_ALL_ANNONCES_BELOW_RECHERCHE = "PAGE_ALL_ANNONCES_BELOW_RECHERCHE";
    const PAGE_ALL_ANNONCES_BETWEN_ANNONCES = "PAGE_ALL_ANNONCES_BETWEN_ANNONCES";
    const PAGE_ALL_ANNONCES_ENTER_ANNONCES  = "PAGE_ALL_ANNONCES_ENTER_ANNONCES";

    const PAGE_ANNONCE_UNDER_TITLE    = "PAGE_ANNONCE_UNDER_TITLE";  
    const PAGE_ANNONCE_BELOW_ANNONCE  = "PAGE_ANNONCE_BELOW_ANNONCE";
    const PAGE_ANNONCE_UNDER_CONTACT  = "PAGE_ANNONCE_UNDER_CONTACT";
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="page", type="string", length=50)
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

     /**
     * @Vich\UploadableField(mapping="images_publicity", fileNameProperty="image")
     * @var File
     */
     private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="script", type="text")
     */
    private $script;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255)
     */
    private $position;
     /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt;

    /**
     * @var string
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt;
    

    public function getConstType(){
        return [
            self::TYPE_PUB_SCRIPT    =>  self::TYPE_PUB_SCRIPT ,
            self::TYPE_PUB_IMAGE_URL =>  self::TYPE_PUB_IMAGE_URL,
        ];
    }
    public function getConstPage(){
        return [
            self::PAGE_PRINCIPALE    => self::PAGE_PRINCIPALE,
            self::PAGE_ALL_ANNONCES  => self::PAGE_ALL_ANNONCES,
            self::PAGE_ANNONCE       => self::PAGE_ANNONCE ,
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PublicityPosition
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set page
     *
     * @param string $page
     *
     * @return PublicityPosition
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return PublicityPosition
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return PublicityPosition
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set script
     *
     * @param string $script
     *
     * @return PublicityPosition
     */
    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script
     *
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return PublicityPosition
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }
     /**
     * @param array $path
     */
    public function setImageFile($image = null)
    {
        if (is_array($image) && $image) {
            $image = $image[0];
        }
        if ($image instanceof File) {
            $this->imageFile = $image;
            $this->updatedAt = new \Datetime();
        }
    }
 

    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return PublicityPosition
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PublicityPosition
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
