<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if ($options['isVetrine']) {
            $builder->add('namevetrine', null, [
                'attr' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Slug Vetrine',
                    'required'    => true,
                ],
            ]);
        }

        $builder
            ->add('firstName', null, [
                'attr' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Prenom',
                    'required'    => true,
                ],
            ])
            ->add('lastName', null, [
                'attr' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Nom',
                    'required'    => true,
                ],
            ])
            ->add('phone', null, [
                'attr' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Tél',
                    'required'    => true,
                ],
            ])
            ->add('email', null, [
                'attr' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Email',
                    'required'    => true,
                ],
            ])
            ->add('plainPassword', Type\RepeatedType::class, [
                'type'            => Type\PasswordType::class,
                'error_bubbling'  => true,
                'options'         => array('translation_domain' => 'FOSUserBundle'),
                'first_options'   => array('attr' => ['class' => 'form-control',
                    'placeholder'                                 => 'Mot de passe',
                    'required'                                    => $options['required'],
                ],
                ),
                'second_options'  => array('attr' => ['class' => 'form-control',
                    'placeholder'                                 => 'confirmer le mot de passe',
                    'required'                                    => $options['required'],
                ],
                ),
                'invalid_message' => 'Non concordance des mots de passe',
            ])
            ->add('enabled');

        if ($options["normalOrVetrine"]) {
            $builder
                ->add('solde', null, [
                    'attr' => [
                        'class'       => 'form-control',
                        'placeholder' => 'Solde DH',
                    ],
                ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'AppBundle\Entity\User',
            'normalOrVetrine' => false,
            'required'        => true,
            'isVetrine'       => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }

}
