<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaidOptionsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('value', null, [
            'attr'  => ['class' => 'form-control',
                'placeholder'       => 'Nombre des jours',
                'required'          => true,
            ],
            'label' => false,
        ])->add('price', null, [
            'attr'  => ['class' => 'form-control',
                'placeholder'       => 'Prix',
                'required'          => false,
            ],
            'label' => false,
        ])->add('title', null, [
            'attr'  => ['class' => 'form-control',
                'placeholder'       => 'Titre',
                'required'          => false,
            ],
            'label' => false,
        ])->add('description', TextareaType::class, [
            'attr'  => ['class' => 'form-control',
                'placeholder'       => 'Description',
                'required'          => false,
            ],
            'label' => false,
        ]);
    }

    public function configuration(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PaidOptions',
        ));
    }

    public function getBlockPrefix()
    {
        return 'appbundle_paidoptions';
    }
}
