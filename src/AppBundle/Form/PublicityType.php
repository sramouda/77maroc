<?php

namespace AppBundle\Form;

use AppBundle\Entity\PublicityPosition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PublicityType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('type', Type\ChoiceType::class, [
                'label'    => false,
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'attr'     => ['class' => 'form-control type-pub'],
                'choices'  => PublicityPosition::getConstType(),

            ])
            ->add('script', Type\TextareaType::class, ['label' => false,
                'attr'                                             => [
                    'class' => 'form-control script-pub',
                    'rows'  => '5',
                ],
            ])->setRequired(false)
            ->add('url', null, ['label' => false,
                'attr'                      => [
                    'class' => 'form-control url-pub',
                ],
            ])->setRequired(false)
            ->add('imageFile', Type\FileType::class, ['label' => false,
                'attr'                                            => [
                    'class' => 'img-pub',
                ],
            ])->setRequired(false)

            ->add('currentURL', Type\HiddenType::class, [
                'mapped' => false,
                'data'   => $options['currentURL'],
            ]);

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PublicityPosition',
            'currentURL' => '',

        ));
        $resolver->setRequired([
            'currentURL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_publicity';
    }

}
