<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageTemplateType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', null, ['attr' => ['class' => 'form-control', 'placeholder' => 'Titre de message']])
            ->add('template', null, ['attr' => ['class' => 'ckeditor']])
            ->add('currentURL', Type\HiddenType::class, [
                'mapped' => false,
                'data'   => $options['currentURL'],
            ]);

    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\MessageTemplate',
            'currentURL' => '',
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_messagetemplate';
    }

}
