<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function menuDashboard(FactoryInterface $factory, array $options)
    {
        $userCurrent = $this->container->get('security.token_storage')->getToken()->getUser();

        $menu = $factory->createItem('root');

        $menu->setChildrenAttribute('class', 'nav');

        $menu->addChild('Dashboard', [
            'route' => 'dashboard_index',
        ]
        )->setAttribute('icon', 'dashboard');

        $permissionsBoites = [
            'PERMISSION_BOITE_ONE',
            'PERMISSION_BOITE_TWO',
            'PERMISSION_BOITE_THREE',
            'PERMISSION_BOITE_FOUR',
            'PERMISSION_BOITE_FIVE',
            'PERMISSION_BOITE_SIX',
        ];

        if (!empty(array_intersect($userCurrent->getRoles(), $permissionsBoites))
            || $userCurrent->hasRole('ROLE_SUPER_ADMIN')
        ) {
            $menu->addChild('Boites annonces', [
                'attributes' => [
                    'dropdown' => true,
                    'href'     => '#boiteannonce',
                    'class'    => "boiteannonce",
                ],
            ]
            )->setAttribute('icon', 'widgets');

            if ($userCurrent->hasRole('PERMISSION_BOITE_ONE') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
                $menu['Boites annonces']->addChild('Boite 1', [
                    'route'           => 'box_ad_index',
                    'routeParameters' => array('id' => 1),
                ]
                )->setAttribute('icon', 'subdirectory_arrow_right');
            }

            if ($userCurrent->hasRole('PERMISSION_BOITE_TWO') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
                $menu['Boites annonces']->addChild('Boite 2', [
                    'route'           => 'box_ad_index',
                    'routeParameters' => array('id' => 2),
                ]
                )->setAttribute('icon', 'subdirectory_arrow_right');
            }

            if ($userCurrent->hasRole('PERMISSION_BOITE_THREE') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
                $menu['Boites annonces']->addChild('Boite 3', [
                    'route'           => 'box_ad_index',
                    'routeParameters' => array('id' => 3),
                ]
                )->setAttribute('icon', 'subdirectory_arrow_right');

            }
            if ($userCurrent->hasRole('PERMISSION_BOITE_FOUR') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
                $menu['Boites annonces']->addChild('Boite 4', [
                    'route'           => 'box_ad_index',
                    'routeParameters' => array('id' => 4),
                ]
                )->setAttribute('icon', 'subdirectory_arrow_right');

            }
            if ($userCurrent->hasRole('PERMISSION_BOITE_FIVE') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
                $menu['Boites annonces']->addChild('Boite 5', [
                    'route'           => 'box_ad_index',
                    'routeParameters' => array('id' => 5),
                ]
                )->setAttribute('icon', 'subdirectory_arrow_right');

            }
            if ($userCurrent->hasRole('PERMISSION_BOITE_SIX') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
                $menu['Boites annonces']->addChild('Boite 6', [
                    'route'           => 'box_ad_index',
                    'routeParameters' => array('id' => 6),
                ]
                )->setAttribute('icon', 'subdirectory_arrow_right');

            }

        }

/**end mod**/

        $permissionsVetrine = [
            'PERMISSION_USERS_VETRINE_ACTIVE',
            'PERMISSION_USERS_VETRINE_EXPIRED',
            'PERMISSION_USERS_REQUEST_VETRINE',
        ];

        if (!empty(array_intersect($userCurrent->getRoles(), $permissionsVetrine))
            || $userCurrent->hasRole('ROLE_SUPER_ADMIN')
        ) {
            $menu->addChild('Vetrine', [
                'attributes' => [
                    'dropdown' => true,
                    'href'     => '#Vetrine',
                    'class'    => "Vetrine",
                ],
            ]
            )->setAttribute('icon', 'widgets');

            if ($userCurrent->hasRole('PERMISSION_USERS_VETRINE_ACTIVE') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
                $menu['Vetrine']->addChild('Vetrines Actives', [
                    'route' => 'users_vetrine_list',
                ]
                )->setAttribute('icon', 'subdirectory_arrow_right')->setExtra('routes', ['users_vetrine_list', 'users_vetrine_add', 'users_vetrine_edit']);
            }
            if ($userCurrent->hasRole('PERMISSION_USERS_REQUEST_VETRINE') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
                $menu['Vetrine']->addChild('Demandes Vetrine', ['uri' => '#'])->setAttribute('icon', 'subdirectory_arrow_right');
            }

            if ($userCurrent->hasRole('PERMISSION_USERS_VETRINE_EXPIRED') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
                $menu['Vetrine']->addChild('Vetrine expires', ['uri' => '#'])->setAttribute('icon', 'subdirectory_arrow_right');

            }

        }

        if ($userCurrent->hasRole('PERMISSION_USERS_NORMAL') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
            $menu->addChild('Normale', [
                'route' => 'users_noraml_list',
            ]
            )->setAttribute('icon', 'reorder')->setExtra('routes', ['users_noraml_list', 'users_normal_add', 'users_normal_edit']);
        }

        if ($userCurrent->hasRole('PERMISSION_BOX_ADS') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
            $menu->addChild('Boites Annonces', [
                'route' => 'boiteAnnonce_list',
            ]
            )->setAttribute('icon', 'inbox');
        }

        if ($userCurrent->hasRole('PERMISSION_ACCOUNT_TYPE') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
            $menu->addChild('Type Comptes', ['route' => 'account_type_list'])->setAttribute('icon', 'assignment_ind');
        }
        if ($userCurrent->hasRole('ROLE_SUPER_ADMIN')) {
            $menu->addChild('Admins', [
                'route' => 'users_admins_list',
            ]
            )->setAttribute('icon', 'supervisor_account')->setExtra('routes', [
                'users_admins_list', 'users_admins_add',
                'users_admins_edit', 'users_admins_perimission',
            ]);
        }
     
      
        if ($userCurrent->hasRole('ROLE_SUPER_ADMIN') || $userCurrent->hasRole('ROLE_ADMIN_SITE_PUBLICITY')) {
            $menu->addChild('Configuration publicité', ['route' => 'index_publicity'])->setAttribute('icon', 'border_top');
        }



        /** Menu General Parameter **/

        if ($userCurrent->hasRole('ROLE_ADMIN_PARAMETER_GENEREUX') || $userCurrent->hasRole('ROLE_SUPER_ADMIN')) {

            $menu->addChild('Parametrage Generale', [
                'attributes' => [
                    'dropdown' => true,
                    'href'     => '#GeneralMenus',
                    'class'    => "GeneralMenus",
                ],
            ]
            )->setAttribute('icon', 'settings');

            $menu['Parametrage Generale']->addChild('Paramètres généraux', [
                'route' => 'general_parameter_index',
            ]
            )->setAttribute('icon', 'perm_data_setting')->setExtra('routes', ['general_parameter_index']);

            $menu['Parametrage Generale']->addChild('Options payantes', [
                'route' => 'paidoptions_list_index',
            ]
            )->setAttribute('icon', 'payment')->setExtra('routes', ['paidoptions_list_index']);

        }

        if ($userCurrent->hasRole('ROLE_SUPER_ADMIN') || $userCurrent->hasRole('ROLE_ADMIN_MESSAGE_TEMPLATE')) {
            $menu->addChild('Configuration Messages', ['route' => 'index_message_template'])->setAttribute('icon', 'comment');
        }

        return $menu;

    }
}
